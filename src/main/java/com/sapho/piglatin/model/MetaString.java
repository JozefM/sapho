package com.sapho.piglatin.model;

import java.util.Objects;

import static com.sapho.piglatin.model.MetaString.State.*;

public class MetaString {

    private final int order;
    private final String original;
    private final String result;
    private final State state;

    public MetaString(int order,
                      String original,
                      String result,
                      State state) {
        Objects.requireNonNull(original);
        Objects.requireNonNull(result);
        Objects.requireNonNull(state);

        this.order = order;
        this.original = original;
        this.result = result;
        this.state = state;
    }

    public int getOrder() {
        return order;
    }

    public String getOriginal() {
        return original;
    }

    public String getResult() {
        return result;
    }

    public boolean isProcessed() {
        return this.state.equals(PROCESSED);
    }

    public boolean isUnprocessed() {
        return this.state.equals(UNPROCESSED);
    }

    public boolean isBeingProcessed() {
        return this.state.equals(PROCESSING);
    }

    public State getState() {
        return state;
    }

    public enum State {
        UNPROCESSED,
        PROCESSING,
        PROCESSED
    }
}
