package com.sapho.piglatin.utils;

import com.sapho.piglatin.model.MetaString;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public interface WithStringUtils {

    String vowels = "aeiouAEIOU";
    String noLettersRegex = "[^a-zA-Z]";

    default boolean beginsWithVowel(String s) {

        if ("".equals(s))
            return false;

        return vowels.contains(String.valueOf(s.charAt(0)));
    }

    default boolean beginsWithConsonant(String s) {

        if ("".equals(s))
            return false;

        return !beginsWithVowel(s);
    }

    default String removeSpecialChars(String s) {
        return s.replaceAll(noLettersRegex, "");
    }

    default String getLowercaseStringToProcess(MetaString metaString) {
        return getStringToProcess(metaString).toLowerCase();
    }

    default String getStringToProcess(MetaString metaString) {
        return metaString.isBeingProcessed() ? metaString.getResult() : metaString.getOriginal();
    }

    default List<Integer> getCapitalLettersIndices(String s) {

        if ("".equals(s))
            return new ArrayList<>();

        return IntStream
                .range(0, s.length())
                .filter(i -> Character.isUpperCase(s.charAt(i)))
                .boxed()
                .collect(toList());
    }

    default List<SimpleEntry<Integer, Character>> getSpecialCharsWithIndices(String s) {

        if ("".equals(s))
            return new ArrayList<>();

        return IntStream
                .range(0, s.length())
                .filter(i -> Character.toString(s.charAt(i)).matches(noLettersRegex))
                .mapToObj(i -> new SimpleEntry<>(i, s.charAt(i)))
                .collect(toList());
    }

    default String reverse(String original) {
        return new StringBuilder(original).reverse().toString();
    }


}
