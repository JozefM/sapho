package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import java.util.Arrays;

import static com.sapho.piglatin.model.MetaString.State.UNPROCESSED;

public class WordSeparatorProcessor extends StringProcessor implements WithStringUtils {

    private String separator;

    public WordSeparatorProcessor(String separator) {
        this.separator = separator;
    }

    @Override
    public MetaString process(MetaString input) {

        if (!input.isProcessed())
            return Arrays.stream(getStringToProcess(input).split(separator))
                    .map(s -> new MetaString(input.getOrder(), s, "", UNPROCESSED))
                    .map(this::processInternal)
                    .reduce((s1, s2) ->
                            new MetaString(s1.getOrder(),
                                    s1.getOriginal(),
                                    s1.getResult() + separator + s2.getResult(),
                                    s1.getState()))
                    .orElse(input);
        else
            return input;
    }

    @Override
    protected MetaString processInternal(MetaString input) {
        return processNext(input);
    }
}
