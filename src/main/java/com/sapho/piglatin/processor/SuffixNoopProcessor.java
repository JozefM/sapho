package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import static com.sapho.piglatin.model.MetaString.State.PROCESSED;

public class SuffixNoopProcessor extends StringProcessor implements WithStringUtils {

    private String suffix;

    public SuffixNoopProcessor(String caseInsensitiveSuffix) {
        this.suffix = caseInsensitiveSuffix.toLowerCase();
    }

    @Override
    protected MetaString processInternal(MetaString input) {

        if (getLowercaseStringToProcess(input).endsWith(suffix))
            return new MetaString(input.getOrder(),
                    input.getOriginal(),
                    input.getOriginal(),
                    PROCESSED);
        else
            return input;
    }
}
