package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import static com.sapho.piglatin.model.MetaString.State.PROCESSING;

public class VowelProcessor extends StringProcessor implements WithStringUtils {

    @Override
    protected MetaString processInternal(MetaString input) {

        if (beginsWithVowel(input.getOriginal())) {
            String toProcess = getLowercaseStringToProcess(input);
            toProcess = removeSpecialChars(toProcess);
            toProcess += "way";

            return new MetaString(input.getOrder(),
                    input.getOriginal(),
                    toProcess,
                    PROCESSING);
        } else
            return input;
    }
}
