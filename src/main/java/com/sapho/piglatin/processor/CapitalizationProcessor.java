package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import java.util.List;

public class CapitalizationProcessor extends StringProcessor implements WithStringUtils {

    @Override
    protected MetaString processInternal(MetaString input) {

        if (input.isBeingProcessed()) {

            String onlyLetters = removeSpecialChars(input.getOriginal());
            List<Integer> capitalsIndices = getCapitalLettersIndices(onlyLetters);
            StringBuilder sb = new StringBuilder(input.getResult());
            capitalsIndices.forEach(i -> sb.setCharAt(i, Character.toUpperCase(sb.charAt(i))));

            return new MetaString(input.getOrder(),
                    input.getOriginal(),
                    sb.toString(),
                    input.getState());

        }

        return input;
    }
}
