package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;

public class PunctuationProcessor extends StringProcessor implements WithStringUtils {

    @Override
    protected MetaString processInternal(MetaString input) {

        if (input.isBeingProcessed()) {

            String reversed = reverse(input.getOriginal());
            List<SimpleEntry<Integer, Character>> specialChars = getSpecialCharsWithIndices(reversed);
            StringBuilder sb = new StringBuilder(reverse(input.getResult()));
            specialChars.forEach(pair -> {
                String stringSoFar = sb.toString();
                String withSpecialChar = stringSoFar.substring(0, pair.getKey()) + pair.getValue() + stringSoFar.substring(pair.getKey());
                sb.replace(0, sb.length(), withSpecialChar);
            });

            sb.reverse();

            return new MetaString(input.getOrder(),
                    input.getOriginal(),
                    sb.toString(),
                    input.getState());

        }

        return input;
    }
}
