package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.utils.WithStringUtils;

import static com.sapho.piglatin.model.MetaString.State.PROCESSING;

public class ConsonantProcessor extends StringProcessor implements WithStringUtils {

    @Override
    protected MetaString processInternal(MetaString input) {

        if (beginsWithConsonant(input.getOriginal())) {
            String toProcess = getLowercaseStringToProcess(input);
            toProcess = removeSpecialChars(toProcess);
            StringBuilder sb = new StringBuilder();
            sb.append(toProcess.substring(1));
            sb.append(toProcess.charAt(0));
            sb.append("ay");

            return new MetaString(input.getOrder(),
                    input.getOriginal(),
                    sb.toString().toLowerCase(),
                    PROCESSING);
        } else
            return input;
    }
}
