package com.sapho.piglatin.processor;

import com.sapho.piglatin.model.MetaString;

public abstract class StringProcessor {

    private StringProcessor next;

    public MetaString process(MetaString input) {

        MetaString result = input;

        if (!input.isProcessed()) {
            result = processInternal(input);
        }

        return processNext(result);
    }

    abstract protected MetaString processInternal(MetaString input);

    public StringProcessor getNext() {
        return next;
    }

    public void next(StringProcessor next) {
        this.next = next;
    }

    public MetaString processNext(MetaString metaString) {

        if (next != null)
            return next.process(metaString);
        else
            return metaString;

    }

}
