package com.sapho.piglatin;

import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.pipeline.StringProcessingPipeline;
import com.sapho.piglatin.processor.*;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import static com.sapho.piglatin.model.MetaString.State.UNPROCESSED;
import static java.util.stream.Collectors.toList;

public class PigLatin {

    public static void main(String[] args) {

        System.out.println("Waiting for user input (to quit, type 'exit'):");
        Scanner in = new Scanner(System.in);

        while (in.hasNextLine()) {
            String input = in.nextLine();
            if("exit".equals(input))
                break;

            if("".equals(input)){
                input = "Hello apple stairway can’t end. this-thing Beach McCloud";
                System.out.println("Empty line can not be submitted for processing. " +
                        "Using a default string: " + input);
            }

            String[] inputArray = input.split(" ");
            List<MetaString> metaStrings = IntStream.range(0, inputArray.length)
                    .mapToObj(i -> new MetaString(i, inputArray[i], "", UNPROCESSED))
                    .collect(toList());

            StringProcessingPipeline pipeline = createPipeline(metaStrings);

            String result = pipeline.startPipeline();
            System.out.println("Resulting sentence: " + result + "\n");
            System.out.println("Waiting for user input:");

        }
    }

    private static StringProcessingPipeline createPipeline(List<MetaString> source) {
        return StringProcessingPipeline.builder()
                .first(new WordSeparatorProcessor("-"))
                .next(new SuffixNoopProcessor("way"))
                .next(ConsonantProcessor::new)
                .next(VowelProcessor::new)
                .next(CapitalizationProcessor::new)
                .next(PunctuationProcessor::new)
                .source(source)
                .build();
    }
}
