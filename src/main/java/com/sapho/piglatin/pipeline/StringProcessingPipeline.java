package com.sapho.piglatin.pipeline;


import com.sapho.piglatin.model.MetaString;
import com.sapho.piglatin.processor.StringProcessor;

import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import static java.util.stream.Collectors.joining;

public class StringProcessingPipeline {

    private StringProcessor first;
    private List<MetaString> source;

    private StringProcessingPipeline(StringProcessor first, List<MetaString> source) {
        this.first = first;
        this.source = source;
    }

    public String startPipeline() {
        return source.stream()
                .map(this.first::process)
                .sorted(Comparator.comparingInt(MetaString::getOrder))
                .map(MetaString::getResult)
                .collect(joining(" "));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private StringProcessor first;
        private StringProcessor buffer;
        private List<MetaString> source;

        public Builder first(StringProcessor processor) {
            if (first == null)
                this.first = processor;

            return this;
        }

        public Builder next(Supplier<StringProcessor> supplier) {
            return next(supplier.get());
        }

        public Builder next(StringProcessor processor) {
            if (first == null)
                throw new RuntimeException("Pipeline chain has to start with a call to first().");

            if (buffer == null)
                first.next(processor);
            else
                buffer.next(processor);

            this.buffer = processor;
            return this;
        }

        public Builder source(List<MetaString> source) {
            this.source = source;
            return this;
        }

        public StringProcessingPipeline build() {

            if (first == null || source == null)
                throw new RuntimeException("Processing pipeline has no processors, or is missing a source.");

            return new StringProcessingPipeline(this.first, this.source);
        }

    }
}
